import json

move_list = []
blocks = None  # Initialize blocks to None

# Read the content of the text file into the 'blocks' variable
with open("file.txt", 'r') as file:
    content_lines = file.readlines()
    blocks = ''.join(content_lines).strip().split("#")

for block in blocks[1:]:
    move = {}
    current_key = None
    current_value = ""

    for line in block.strip().split('\n'):
        try:
            key, value = line.strip().split(':', 1)
            if current_key:
                # If there is a previous key, check if 'Tipo de Concurso' is part of the value
                if 'Tipo de Concurso' in current_value:
                    # Append the whole line as part of the 'Efecto' value
                    current_value += " " + line.strip()
                else:
                    # Otherwise, add the previous key-value pair to the move dictionary
                    move[current_key] = current_value.strip()
                current_key = key.strip()
                current_value = value.strip()
            else:
                current_key = key.strip()
                current_value = value.strip()
        except ValueError:
            # If there is no colon in the line, it means it's a continuation of the previous value
            current_value += " " + line.strip()

    # Append the last value to the 'Efecto' property of the move
    if current_key:
        move[current_key] = current_value.strip()

    # Append move to the list
    move_list.append(move)

# Save the extracted data as JSON
json_file_path = 'TXToutput.json'
with open(json_file_path, 'w') as json_file:
    json.dump(move_list, json_file, indent=4, ensure_ascii=False)  # Indent for pretty formatting (optional)

print("JSON file TXToutput.json created successfully.")
