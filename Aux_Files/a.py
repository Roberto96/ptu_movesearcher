# Read the content from the file
with open("moves.txt", "r") as file:
    lines = file.readlines()

# Modify the content (add '#' before each occurrence of "Movimiento:")
modified_lines = [line.replace("Movimiento:", "# Movimiento:") for line in lines]

# Write the modified content back to the same file (file.txt)
with open("file.txt", "w") as file:
    file.writelines(modified_lines)

