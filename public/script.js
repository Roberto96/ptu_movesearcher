const display = document.querySelector("#results");
const Input_Movimiento = document.querySelector("#Movimiento");
const changeableBg = document.getElementById('bgcolormode');
const buttonImg = document.getElementById('button-img');
const searchButtonImg = document.getElementById('searchButtonImg');
const searchButton = document.getElementById('searchButton');
const lanButton = document.getElementById('language-selector');
const fetchSpanishDB = 'jsondb/MoveListESP.json';
const fetchEnglishDB = 'jsondb/MoveListENG.json';
let aux_buttonImg = buttonImg;
let isdarkmode=false;



//searchbar
let query_movimiento = Movimiento.value;
let query_tipo = Tipo.value;
let query_clase = Clase.value;
let query_cp = CP.value;
let query_db = DB.value;
let query_frecuencia = Frecuencia.value;

startondarkmode();


function fetching(db){

  //show movelist & and get movelist
  fetch(db)
        .then(response => response.json())
        .then(data => {
          let displayMoveList = data.filter((allTheData) => {
    
            // Check if the data matches both the Clase and Tipo criteria
            return (
              (query_movimiento === "" || allTheData.Movimiento.toLowerCase().includes(query_movimiento.toLowerCase())) &&
              (query_tipo === "" || allTheData.Tipo.toLowerCase().includes(query_tipo.toLowerCase())) &&
              (query_clase === "" || allTheData.Clase.toLowerCase().includes(query_clase.toLowerCase())) &&
              (query_cp === "" || allTheData.CP.toLowerCase().includes(query_cp.toLowerCase())) &&
              (query_db === "" || allTheData.DB.toLowerCase().includes(query_db.toLowerCase())) &&
              (query_frecuencia === "" || allTheData.Frecuencia.toLowerCase().includes(query_frecuencia.toLowerCase())) 
            );
          }).map((object) => {
            const { Movimiento, Tipo, Frecuencia, CP, DB, Clase, Rango, Efecto } = object;

            return `
              <div class="container-item-result">
              <div class="row-item-Movimiento">${Movimiento}</div>
              <div class="row-item-Tipo"><img src="Img/tipos/${Tipo}.png"></div>
              <div class="row-item-Frecuencia">${Frecuencia}</div>
              <div class="row-item-CP">${CP}</div>
              <div class="row-item-DB">${DB}</div>
              <div class="row-item-Clase"><img src="Img/clase/${Clase}.png"></div>
              <div class="row-item-Rango">${Rango}</div>
              <div class="row-item-Efecto">${Efecto}</div>
            </div>
              `;

              
          }).join("");

          display.innerHTML = displayMoveList;
          
        })
        .catch(error => {
          console.error('Error reading the JSON file:', error);
        });
  }    

function search(){
   query_movimiento = Movimiento.value
   query_tipo = Tipo.value;
   query_clase = Clase.value;
   query_cp = CP.value;
   query_db = DB.value;
   query_frecuencia = Frecuencia.value;

   //Check for valid values
   if (query_cp === -1 ){query_clase = ""}
   else if(query_cp === 0){query_clase = "--"}

   if (query_db === -1 ){query_db = ""}
   else if(query_db === 0){query_db = "--"}

   if (lanButton.value == "english"){
    fetching(fetchEnglishDB);
   }else{
    fetching(fetchSpanishDB);
   }

  
}


Input_Movimiento.addEventListener("input", () => {
  search();
});

function startondarkmode(){
  changeableBg.classList.toggle('lightmode'); 
  changeableBg.classList.toggle('darkmode'); 
  searchButton.classList.toggle('frickingbutton')
  searchButton.classList.toggle('frickingbutton-dark')
  buttonImg.src = 'Img/modo/sol.png'; 
  searchButtonImg.src = "Img/pokedex-on.png"
  isdarkmode = true;
  
}
  
function darkmode() {
  changeableBg.classList.toggle('lightmode'); 
  changeableBg.classList.toggle('darkmode'); 
  searchButton.classList.toggle('frickingbutton')
  searchButton.classList.toggle('frickingbutton-dark')
  // Toggle the image source based on the current background color class
  if (changeableBg.classList.contains('lightmode')) {
    buttonImg.src = 'Img/modo/luna-hover.png'; // Set to light
    searchButtonImg.src = "Img/pokedex.png"
    isdarkmode = false;
  } else {
    buttonImg.src = 'Img/modo/sol-hover.png'; // Set to dark
    searchButtonImg.src = "Img/pokedex-on.png"
    isdarkmode = true;
  }
}

function changeDarkmodeImg(hover){
 if(hover && isdarkmode){
   buttonImg.src = 'Img/modo/sol-hover.png';
 }else if(isdarkmode){
   buttonImg.src = 'Img/modo/sol.png';
 }else if(hover){
   buttonImg.src = 'Img/modo/luna-hover.png';
 }else{
   buttonImg.src = 'Img/modo/luna.png';
 }

}

//Language implementation
const languageSelector = document.getElementById('language-selector');
const translateElements = document.querySelectorAll('.translate');

// Function to load the language file and update content
function loadLanguageFile(language) {
  fetch(`lang/${language}.json`)
    .then(response => response.json())
    .then(data => {
      translateElements.forEach(element => {
        const translationKey = element.dataset.key;
        element.textContent = data[translationKey];

        if(language==="english"){
          fetching(fetchEnglishDB);
          lanButton.value = 'english';
          Input_Movimiento.placeholder='Ex: Tackle, Surf, Toxic...';
          
        }else{
          fetching(fetchSpanishDB);
          lanButton.value = 'español';
          Input_Movimiento.placeholder='Ej: Placaje, Surf, Rayo...';
        }
        optionsLanguageChanger(language)
        setLanguageCookie(language);

      });
    });
}

// Event handler for language selector button
languageSelector.addEventListener('change', function () {
  const selectedLanguage = this.value; // Assume the value represents the language (e.g., 'spanish' or 'english')
  query_tipo = "";
  query_clase = "";
  query_cp = "";
  query_db = "";
  query_frecuencia = "";
  
  loadLanguageFile(selectedLanguage);
  

});

// fn to change language of options
function optionsLanguageChanger(language) {
  const tipoDIV = document.getElementById("tipoDIV");
  const frecuenciaDIV = document.getElementById("frecuenciaDIV");
  const claseDIV = document.getElementById("claseDIV");

  if (language === "english") {
    tipoDIV.innerHTML = `
    <label class="thickLabels translate" data-key="labelTipo">Type</label>
    <br><br>
    
    <select id="Tipo">
      <option value="">--</option>
      <option value="Bug">Bug</option>
      <option value="Dark">Dark</option>
      <option value="Dragon">Dragon</option>
      <option value="Electric">Electric</option>
      <option value="Fairy">Fairy</option>
      <option value="Fighting">Fighting</option>
      <option value="Fire">Fire</option>
      <option value="Flying">Flying</option>
      <option value="Ghost">Ghost</option>
      <option value="Grass">Grass</option>
      <option value="Ground">Ground</option>
      <option value="Ice">Ice</option>
      <option value="Normal">Normal</option>
      <option value="Poison">Poison</option>
      <option value="Psychic">Psychic</option>
      <option value="Rock">Rock</option>
      <option value="Steel">Steel</option>
      <option value="Water">Water</option>


    </select>

    `;

    frecuenciaDIV.innerHTML = `
    <label class="thickLabels">Frecuency</label>
      <br><br>
      <select id="Frecuencia">
        <option value="">--</option>
        <option value="At-Will">At-Will</option>
        <option value="EOT">EOT</option>
        <option value="Scene">Scene</option>
        <option value="Daily">Daily</option>
      </select>

    `;

    claseDIV.innerHTML = `
    <label class="thickLabels">Category</label>
    <br><br>
    <select id="Clase"> 
      <option value="">--</option>
      <option value="Physical">Physical</option>
      <option value="Special">Special</option>
      <option value="Status">Status</option>
      <option value="Static">Static</option>
    </select>

    `;


  } else {
    tipoDIV.innerHTML = `
    <label class="thickLabels">Tipo</label>
    <br><br>
    
    <select id="Tipo">
      <option value="">--</option>
      <option value="Acero">Acero</option>
      <option value="Agua">Agua</option>
      <option value="Bicho">Bicho</option>
      <option value="Dragón">Dragón</option>
      <option value="Eléctrico">Eléctrico</option>
      <option value="Fantasma">Fantasma</option>
      <option value="Fuego">Fuego</option>
      <option value="Hada">Hada</option>
      <option value="Hielo">Hielo</option>
      <option value="Lucha">Lucha</option>
      <option value="Normal">Normal</option>
      <option value="Planta">Planta</option>
      <option value="Psíquico">Psíquico</option>
      <option value="Roca">Roca</option>
      <option value="Siniestro">Siniestro</option>
      <option value="Tierra">Tierra</option>
      <option value="Volador">Volador</option>
      <option value="Veneno">Veneno</option>

    </select>

    `;

    frecuenciaDIV.innerHTML = `
    <label class="thickLabels">Frecuencia</label>
      <br><br>
      <select id="Frecuencia">
        <option value="">--</option>
        <option value="A Voluntad">A Voluntad</option>
        <option value="Recarga">Recarga</option>
        <option value="Escena">Escena</option>
        <option value="Diario">Diario</option>
      </select>

    `;

    claseDIV.innerHTML = `
    <label class="thickLabels">Categoría</label>
    <br><br>
    <select id="Clase"> 
      <option value="">--</option>
      <option value="Físico">Físico</option>
      <option value="Especial">Especial</option>
      <option value="Estatus">Estatus</option>
      <option value="Pasivo">Pasivo</option>
    </select>

    `;
  }

  

  
}

  



//Bake Cookie :3
function setLanguageCookie(language) {
  const now = new Date();
  const expirationDate = new Date(now.getTime() + 365 * 24 * 60 * 60 * 1000); // Set the cookie expiration to one year from now
  document.cookie = `language=${encodeURIComponent(language)}; expires=${expirationDate.toUTCString()}; path=/`;
}

//get cookie :3
function getLanguageFromCookie() {
  const cookieName = 'language=';
  const decodedCookie = decodeURIComponent(document.cookie);
  const cookieArray = decodedCookie.split(';');

  for (let i = 0; i < cookieArray.length; i++) {
    let cookie = cookieArray[i];
    while (cookie.charAt(0) === ' ') {
      cookie = cookie.substring(1);
    }
    if (cookie.indexOf(cookieName) === 0) {
      return cookie.substring(cookieName.length, cookie.length);
    }
  }

  return null; // Return null if the language cookie is not found :(
}

//page intial loading
document.addEventListener('DOMContentLoaded', function () {
  const selectedLanguage = getLanguageFromCookie();
  if (selectedLanguage === null) { // Check for null (no language cookie found)
    loadLanguageFile("english"); // Load English as the default language
  } else {
    loadLanguageFile(selectedLanguage);
  }
});